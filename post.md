It's fall and that means it's time for another Less Wrong Darwin Game. This year, you'll be designing one (or more) species of organisms that will compete with each other for food including (sometimes) eating each other.

[**Click here to participate**](https://docs.google.com/forms/d/e/1FAIpQLSeCg0OB2h0Bg1HBez2-0Eklv-w6NZm4JCppTu3TdP3E6gXt4w/viewform?usp=sf_link) You have one week from September 23 to design your species. Submit them by September 30th or earlier.

Each player starts with a population of organisms. Each round each of your organisms will be randomly paired with another organism. At this point, one of two things will happen:
- If one organism can eat the other organism then it will do so.
- If nobody gets eaten then both organisms get an opportunity to forage for plants.

After everyone has eaten, each organism will attempt to reproduce. The more an organism eats you eat the most descendents an organism can leave.

# Food

Each round your organisms lose 20% of their energy to metabolism resulting (on average) in a 20% decrease in population. You must eat food to counteract metabolism. There are two sources of food: plants and other animals.

## Predation

There are two phases to combat. In the first phase organisms size each other up to figure out which is the predator and which is prey. There are two ways for an organism to become the predator.

1. **Venom.** If one organism has venom but the other does not have antivenom then the organism with venom is the predator. (Antivenom is a prerequisite to venom.)
2. **Weapons.** Weapons represent claws, teeth and tusks. If either organism's weapons value exceeds the prey's weapons + armor then the organism with the higher weapons value will become the predator.

Venom takes priority over weapons. Once a predator-prey relationship is established (*if* a predator-prey relationship is established) the prey will get a chance to escape. If the prey's speed equals or exceeds the predator's then nobody gets eaten.

Venom, weapons and antivenom all make your organism bigger, which slows down reproduction.

| Adaptation | Size | Notes |
|---|---|---|
| Venom | 6 | Requires Antivenom |
| Antivenom | 1 | |
| Weapons$\times n$ | $n$ | $n\geq0$ |
|Armor$\times n$ | $\frac{3n}4$ | $n\geq0$ |
| Speed$\times n$ | $n$ | $n\geq0$ |

Omnivores priorize meat over plants, when they can get it *even if foraging for plants would be more metabolically efficient*[^1].

[^1]: This seemingly-irrational behavior has precedent among human beings due to sexual competition. Jared Diamond writes about why in his book *Why Is Sex Fun?: The Evolution of Human Sexuality*.

Predation has an efficiency of 0.8. That means 80% of the prey's energy can be used by the predator.

Only organisms of different species eat each other. Cannibalism is disabled.

## Foraging for Plants

There are various kinds of plant food available. In order to eat each food you'll need the proper digestive system.

| Food | Nutritional Value | Size |
|---|---|---|
| Leaves | 7 | 5 |
| Grass | 6 | 3 |
| Seeds | 5 | 1 |

Whether your organism can digest a particular plant food is a binary value. No organism is better at digesting leaves than any other organism.

There is a tradeoff. The ability to eat leaves/grass/seeds makes your organism bigger which slows down reproduction. Also, there is a finite supply of leaves/grass/seeds. The more *other* organisms are foraging from a plant source, the less advantageous it is for you to forage for it youself.

# Simple Ecosystems

Consider an ecosystem with three kinds of plant food available: seeds, leaves and grass. 1,000 units of each plant food are produced per round.

## Example 1

This all may sound a little confusing but it makes sense once we use some real exampless. Let's start with two species: housecats and mice.

| Species | Weapons | Speed | Eats Seeds? |
|---|---|---|---|
| Housecat | 1 | 2 | No |
| Mouse | 0 | 1 | Yes |

At first, both populations grow. The mice reproduce faster than the cats. Then the cats catch up and eat all of the mice. Having exhausted their food supply, the cat population starves to extinction.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/cat-mouse-1.png)

## Example 2

What happens if we add songbirds? Songbirds fly. They are fast enough to evade cats. But speed costs energy which makes it more expensive for songbirds to breed than for mice to breed. Mice outcompete songbirds in a world without cats.

| Species | Weapons | Speed | Eats Seeds? |
|---|---|---|---|
| Mouse | 0 | 1 | Yes |
| Songbird | 0 | 3 | Yes |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/mouse-songbird-1.png)

We can establish a periodic equilibrium by reintroducing cats. If the mouse population rises too much the cat population rises to eat them, decreasing the mouse population. But there is a limit to how high the cat population can get because the cat population is matched randomly with other organisms and those other organism are often songbirds.

- The mouse population is kept in check by the cat population.
- The cat population is kept in check by the bird population.
- The bird population is kept in check by the mouse population.

| Species | Weapons | Speed | Eats Seeds? |
|---|---|---|---|
| Housecat | 1 | 2 | No |
| Mouse | 0 | 1 | Yes |
| Songbird | 0 | 3 | Yes |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/cat-mouse-songbird-1.png)

## Example 3

What happens if we add a falcon so fast it can catch both mice and songbirds? The falcons and housecats eat all of their prey. Our previous equilibrium has been broken. The ecosystem collapses. Everyone dies.

| Species | Weapons | Speed | Eats Seeds? |
|---|---|---|---|
| Falcon | 1 | 20[^2] | No |
| Housecat | 1 | 2 | No |
| Mouse | 0 | 1 | Yes |
| Songbird | 0 | 3 | Yes |

[^2]: In the real game, weapons, armor and speed will all be limited to 10.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/falcon-mouse-songbird-housecat-1.png)

To restabilize things we need more prey. Let's add koalas. Koalas eat leaves. I made them venomous to protect them from predators.

| Species | Weapons | Speed | Eats Seeds? | Eats leaves? | Venom/Antivenom? |
|---|---|---|---|---|---|
| Falcon | 1 | 20 | No | No | No |
| Housecat | 1 | 2 | No | No | No |
| Mouse | 0 | 1 | Yes | No | No |
| Songbird | 0 | 3 | Yes | No | No |
| Koala | 0 | 0 | No | Yes | Venom |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/housecat-mouse-songbird-falcon-koala.png)

The venomous koalas eat a food source (leaves) disconnected from the rest of the food chain which relies on seeds. Nothing preys on them because they are venomous and no other animals have antivenom. They koals are effectively disconnected from the rest of the ecosystem. They just get in the way of predation by the falcons and the housecats which helps stabalize the seed-based food web.

## Example 4

A world where nothing preys on nor competes with koalas is boring. Let's add owls. Owls really do prey on koalas.

| Species | Weapons | Speed | Eats Seeds? | Eats leaves? | Venom/Antivenom? |
|---|---|---|---|---|---|
| Falcon | 1 | 20 | No | No | No |
| Housecat | 1 | 2 | No | No | No |
| Mouse | 0 | 1 | Yes | No | No |
| Songbird | 0 | 3 | Yes | No | No |
| Koala | 0 | 0 | No | Yes | Venom |
| Owl | 1 | 2 | No | No | Antivenom |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/introducing-owls.png)

Introducing owls gets us exciting population spikes and crashes but it wipes out the mice and housecats.

The problem is owls have too much of an advantage over housecats. Owls and housecats are almost identical. The only difference is antivenom, which is cheap. Koalas are a major food source because only owls prey on them. To give housecats a chance we need something else to prey on koalas that also provides food for housecats.

| Species | Weapons | Speed | Eats Seeds? | Eats leaves? | Venom/Antivenom? |
|---|---|---|---|---|---|
| Falcon | 1 | 20 | No | No | No |
| Housecat | 1 | 2 | No | No | No |
| Mouse | 0 | 1 | Yes | No | No |
| Songbird | 0 | 3 | Yes | No | No |
| Koala | 0 | 0 | No | Yes | Venom |
| Owl | 1 | 2 | No | No | Antivenom |
| Mongoose | 1 | 1 | No | No | Antivenom |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/introducing-the-mongoose.png)

The mongeese do keep the owls in check. I'm starting to think housecats might be a lost cause. Whatever the case, we need a stabalizing animal which thrives when mongeese are present and declines when there are too many koalas. (This is just the songbirds we introduced in the beginning except for leaves/koalas/owls instead of seeds/mice/housecats.)

Giraffes eat leaves just like koalas but they are nonvenomous. I gave giraffes armor instead of weapons because if they had weapons they might eat small animals instead of leaves.

| Species | Weapons | Armor | Speed | Eats Seeds? | Eats leaves? | Venom/Antivenom? |
|---|---|---|---|---|---|---|
| Falcon | 1 | 0 | 20 | No | No | No |
| Housecat | 1 | 0 | 2 | No | No | No |
| Mouse | 0 | 0 | 1 | Yes | No | No |
| Songbird | 0 | 0 | 3 | Yes | No | No |
| Koala | 0 | 0 | 0 | No | Yes | Venom |
| Giraffe | 0 | 2 | 6 | No | Yes | No |
| Owl | 1 | 0 | 2 | No | No | Antivenom |
| Mongoose | 1 | 0 | 1 | No | No | Antivenom |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/introducing-giraffes.png)

Adding giraffes got us mice and housecats back but we lost koalas along with the owls and mongeese that prey on koalas.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/foot-web-1-crossed-out.png)

## Example 5

Our herbivores only eat seeds and leaves. All the grass is going to waste. Lets add some grass eaters to our menagerie.

| Species | Weapons | Armor | Speed | Eats Seeds? | Eats leaves? | Eats Grass? | Venom/Antivenom? |
|---|---|---|---|---|---|---|---|
| Rabbit | 0 | 0 | 0 | Yes | Yes | Yes | No |
| Panda | 0 | 2 | 0 | No | Yes | Yes | No |
| Snake | 0 | 0 | 1 | No | No | No | Yes |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/three-trophic-levels-endure.png)

Out new ecosystem has no more diversity than the previous one. (Only six species survive to equilibrium.) But we have achieved something new. The new ecosystem sustains a food chain three trophic levels deep. Owls eat snakes eat pandas.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/big-web-crossed-out.png)

# Multi-Biome Ecosystems

Let's compare two biomes.

| Name | Seed | Leaf | Grass |
|---|---|---|---|
| Grassland | 100 | 10 | 200 |
| Rainforest | 100 | 200 | 10 |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/grassland-rainforest.png)

Note that I set the overall plant production lower. This results in smaller populations which are more likely to be made extinct by to random fluctions. In the grassland, everything dies but songbirds. Leaves and grass go uneaten. In the rainforest we get a 3-species equilibrium of owls, pandas and mice.

## Example 1

Suppose we give each animal a small chance (proportional to its speed) of wandering to a random biome. Our two biomes are now connected.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/grassland-rainforest-2.png)

This increases biodiversity in ecah particular ecosystem. However, it does not increase overall biodiversity.

## Example 2

What if we add an additional biome?

| Name | Seed | Leaf | Grass |
|---|---|---|---|
| Grassland | 100 | 10 | 200 |
| Rainforest | 100 | 200 | 10 |
| Temperate Forest | 200 | 10 | 100 |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/grassland-rainforest-temperate.png)

Housecats and falcons are back but we lost owls and snakes.

# Surviving the environment

The world is a dangerous place. It's not just starvation and other animals that can kill you. You can only survive in the Ocean/Benthic if you breathe water. You can only survive on land if you breathe air. Both air and water breathers can survive on the Rivers and Coasts.

You do not have to manually set whether your organism breathes air or water. This will be inferred from your spawning location. You cannot start in a river or on a coast.

## Temperature

You need heat tolerance to survive in the desert. You need cold tolerance to survive in the tundra.

| Temperature Adaptation | Size |
|---|---|
| Heat Tolerance | 5 |
| Cold Tolerance | 5 |

Heat and cold tolerance are not useful for aquatic organisms.

# The actual game settings

The actual game is more complicated than my examples. There are more biomes and more plant foods available. I will not be entering my own animals into the game. Only player animals will be included.

| Food | Nutritional Value | Size |
|---|---|---|
| Carrion | 10 | 6 |
| Leaves | 7 | 5 |
| Grass | 6 | 3 |
| Seeds | 5 | 1 |
| Ditritus | 4 | 3 |
| Coconuts | 3 | 4 |
| Algae | 2 | 2 |
| Lichen | 1 | 3 |

## Biomes

| Name | Carrion | Leaves | Grass | Seeds | Ditritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Grassland | 0 | 100 | 1000 | 2000 | 0 | 0 | 0 | 50 |
| Rainforest | 0 | 1000 | 2000 | 100 | 0 | 0 | 0 | 50 |
| Temperate Forest | 0 | 2000 | 100 | 1000 | 0 | 0 | 0 | 50 |
| Ocean | 10 | 0 | 0 | 0 | 10 | 0 | 10000 | 0 |
| Benthic | 10 | 0 | 0 | 0 | 1000 | 0 | 0 | 0 |
| Tundra | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 300 |
| Desert | 100 | 0 | 1 | 1 | 1 | 0 | 0 | 0 |
| Shore | 0 | 0 | 0 | 0 | 20 | 1000 | 1000 | 10 |
| River | 500 | 500 | 500 | 500 | 500 | 500 | 500 | 500 |
| Human Garbage Dump | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |

# Questions and Answers

## How do I win?

Your species survies.

## Can I coordinate with other players?

There is no rule against it.

## Can I betray the other players who think I'm coordinating with them?

There is no rule against it.

## I'm worried my species will do poorly and I'll be publicly shamed.

Use a pseudonym. I will only link to social media if you do well.

## How many species can I enter?

Up to 10. I am relying on the honor system. Please do not abuse it.

## Do I have to be a Less Wrong user to participate?

You do not have to be affiliated with Less Wrong. Anyone is welcome to participate. Invite your friends to play!

## I found a bug in your code.

Please private message me.

## I like your work and would like to support you.

You are under no obligation whatsoever to send donations. My primary objective is to make a fun, educational game for lots of people.

That said, I do like money. You can Venmo me @Lsusr

## What can I win?

Honor and glory. I will link to the winners' social media accounts. (Unless it is something I consider dangerous or object to on moral grounds.)

# Example source code

You can try out different strategies with the source code below. It is run with [hy](https://docs.hylang.org/en/alpha/). You can install hy with pip `$ pip3 install hy`. You will need matplotlib too. Install it with `$ pip3 install matplotlib`.

[Source code available on GitLab](https://gitlab.com/lsusr/darwin)

# How do I participate?

[**Design your species here.**](https://docs.google.com/forms/d/e/1FAIpQLSeCg0OB2h0Bg1HBez2-0Eklv-w6NZm4JCppTu3TdP3E6gXt4w/viewform?usp=sf_link) You have one week from September 23 to design your species. Submit them by September 30th or earlier.
