#!/home/lsusr/.local/bin/hy
(import random
        [math [floor]])
(random.seed 0)

(setv +base-energy+ 0.1)
(setv +species-list+ [])

(defn energy-of [organism]
  (+ +base-energy+
     (if (. organism ["venom"]) 6 0)
     (if (. organism ["antivenom"]) 1 0)
     (if (. organism ["carrion-eater"]) 6 0)
     (if (. organism ["leaf-eater"]) 5 0)
     (if (. organism ["grass-eater"]) 3 0)
     (if (. organism ["seed-eater"]) 1 0)
     (if (. organism ["detritus-eater"]) 3 0)
     (if (. organism ["coconut-eater"]) 4 0)
     (if (. organism ["algae-eater"]) 2 0)
     (if (. organism ["lichen-eater"]) 1 0)
     (if (. organism ["heat"]) 2 0)
     (if (. organism ["cold"]) 2 0)
     (* (. organism ["armor"]) 0.75)
     (. organism ["weapons"])
     (. organism ["speed"])))

(setv +animal-csv+ "csvs/43-species.csv")

(defn row-to-organism [row]
  (setv [timestamp species-name spawning-zone venom weapons armor
         speed foraging temperature human-name contact-info lesswrong social-media] row)
  {"name" species-name
   "spawning-zone" spawning-zone
   "venom" (in "Venom" venom)
   "antivenom" (in "Antivenom" venom)
   "weapons" (int weapons)
   "armor" (int armor)
   "speed" (int speed)
   "breathes" (if (in spawning-zone ["Ocean" "Benthic"])
                "water"
                "air")
   "heat" (in "Heat" temperature)
   "cold" (in "Cold" temperature)
   "carrion-eater" (in "Carrion" foraging)
   "leaf-eater" (in "Leaves" foraging)
   "grass-eater" (in "Grass" foraging)
   "seed-eater" (in "Seeds" foraging)
   "detritus-eater" (in "Detritus" foraging)
   "coconut-eater" (in "Coconuts" foraging)
   "algae-eater" (in "Algae" foraging)
   "lichen-eater" (in "Lichen" foraging)})

(import [hy.models [HySymbol]])
(import csv)
(with [f (open +animal-csv+ :mode "r")]
  (setv csv-file (csv.reader f))
  (next csv-file) ;; skip first row
  (for [row csv-file]
(.append +species-list+ (row-to-organism row))))

(setv +seed-population-energy+ 1000)

(defn initial-population [biome]
  "Creates a list of initial organisms"
  (+ [] #*
     (lfor species +species-list+
           (if (= (. species ["spawning-zone"])
                  biome)
             (do
               (setv organisms [])
               (setv energy +seed-population-energy+)
               (while (> energy 0)
                 (.append organisms (.copy species))
                 (-= energy (energy-of species)))
               organisms)
             []))))

(defn can-survive? [organism biome]
  (and
    (if (in biome.identifier ["Shore" "River"])
      True
      (if (= (. organism ["breathes"]) "water")
        (in biome.identifier
            ["Ocean" "Benthic"])
        (not-in biome.identifier
                ["Ocean" "Benthic"])))
    (or (!= biome.identifier "Desert")
        (. organism ["heat"]))
    (or (!= biome.identifier "Tundra")
        (. organism ["cold"]))))

(defn has-venom? [organism]
  (. organism ["venom"]))

(defn has-antivenom? [organism]
  (. organism ["antivenom"]))

(defn chase-result? [organism-0 organism-1]
  (if
    (> (. organism-0 ["speed"])
       (. organism-1 ["speed"]))
    (return 0)
    (< (. organism-0 ["speed"])
       (. organism-1 ["speed"]))
    (return 1))
  None)

(defn fight-result? [organism-0 organism-1]
  "Returns winner of fight if there is one. Otherwise returns null"
  (if (or (and (has-venom? organism-0)
               (not (has-antivenom? organism-1)))
          (and (has-venom? organism-1)
               (not (has-antivenom? organism-0))))
    (return (if (has-venom? organism-0)
              0
              1)))
  (if
    (> (. organism-0 ["weapons"])
       (+ (. organism-1 ["weapons"])
          (. organism-1 ["armor"])))
    (return 0)
    (< (+ (. organism-0 ["weapons"])
          (. organism-0 ["armor"]))
       (. organism-1 ["weapons"]))
    (return 1))
  None)

(setv +basal-metabolism+ 0.2)
(setv +predatory-efficiency+ 0.95)
(setv +reproduction-constant+ 10)

(defn reproduce [organism prey-energy]
  (setv saved-energy (* (energy-of organism) (- 1.0 +basal-metabolism+)))
  (setv total-energy (+ saved-energy prey-energy))
  (setv reproduction-opportunities (floor (* +reproduction-constant+ total-energy)))
  (setv babies [])
  (for [_ (range reproduction-opportunities)]
    (do
      (setv baby (.copy organism))
      (if (< (random.random) (/ 1
                                (energy-of baby)
                                +reproduction-constant+))
        (.append babies baby))))
  babies)

(defn fight-flight-result? [organism-0 organism-1]
  (setv fight-result (fight-result? organism-0 organism-1))
  (if (= None fight-result)
    (return None))
  (if (= (chase-result? organism-0 organism-1)
         fight-result)
    (return fight-result))
  (return None))

(defn interact [biome organism-0 organism-1]
  (if (= (. organism-0 ["name"]) (. organism-1 ["name"]))
    None
    (do
      (setv fight-flight-result (fight-flight-result? organism-0 organism-1))
      (if (!= None fight-flight-result)
        (return
          (reproduce (. [organism-0 organism-1] [fight-flight-result])
                     (* +predatory-efficiency+
                        (energy-of (. [organism-1 organism-0]
                                      [fight-flight-result]))))))))
  (+ #*
     (lfor organism [organism-0 organism-1]
           (reproduce
             organism
             (if
               (and biome.carrion
                    (. organism ["carrion-eater"]))
               (do (-= biome.carrion 1) 10)
               (and biome.leaves
                    (. organism ["leaf-eater"]))
               (do (-= biome.leaves 1) 7)
               (and biome.grass
                    (. organism ["grass-eater"]))
               (do (-= biome.grass 1) 6)
               (and biome.seeds
                    (. organism ["seed-eater"]))
               (do (-= biome.seeds 1) 5)
               (and biome.detritus
                    (. organism ["detritus-eater"]))
               (do (-= biome.detritus 1) 4)
               (and biome.coconuts
                    (. organism ["coconut-eater"]))
               (do (-= biome.coconuts 1) 3)
               (and biome.algae
                    (. organism ["algae-eater"]))
               (do (-= biome.algae 1) 2)
               (and biome.lichen
                    (. organism ["lichen-eater"]))
               (do (-= biome.lichen 1) 1)
               0)))))

(defn encounters [biome]
  "Creates pairs of encounters from populations"
  (random.shuffle biome.population)
  (+ [] [] #*
     (lfor i (range (// (len biome.population) 2))
           (interact biome
                     (. biome.population [i])
                     (. biome.population [(inc i)])))))

(defn population-to-dict [population]
  (setv output {})
  (for [organism population]
    (if (in (. organism ["name"]) output)
      (+= (. output [(. organism ["name"])]) 1)
      (setv (. output [(. organism ["name"])]) 1)))
  output)

(defn food-chain []
  (import graphviz)
  (setv g (graphviz.Digraph "G" :filename "food-web.gv"))

  (g.attr "node" :shape "rectangle" :style "radial" :fillcolor "lightgreen")
  (g.node "carrion")
  (g.node "leaves")
  (g.node "grass")
  (g.node "seeds")
  (g.node "detritus")
  (g.node "coconuts")
  (g.node "algae")
  (g.node "lichen")

  (g.attr "node" :shape "oval" :fillcolor "white")
  (for [species +species-list+]
    (setv organism (species))
    (if organism.carrion-eater
      (g.edge "carrion" (. species ["name"])))
    (if organism.leaf-eater
      (g.edge "leaves" (. species ["name"])))
    (if organism.grass-eater
      (g.edge "grass" (. species ["name"])))
    (if organism.seed-eater
      (g.edge "seeds" (. species ["name"])))
    (if organism.detritus-eater
      (g.edge "detritus" (. species ["name"])))
    (if organism.coconut-eater
      (g.edge "coconuts" (. species ["name"])))
    (if organism.algae-eater
      (g.edge "algae" (. species ["name"])))
    (if organism.lichen-eater
      (g.edge "lichen" (. species ["name"]))))
  (for [species-0 +species-list+]
    (for [species-1 +species-list+]
      (if (= 1 (fight-flight-result? (species-0) (species-1)))
        (g.edge (. species-0 ["name"])
                (. species-1 ["name"]))
        )))
  (g.render "out" :format "png")
  #_(g.view))

;(food-chain)

(defclass Biome []
  (defn --init-- [self identifier
                  carrion-production
                  leaf-production
                  grass-production
                  seed-production
                  detritus-production
                  coconut-production
                  algae-production
                  lichen-production]
    (setv self.population (initial-population identifier)
          self.identifier identifier)
    (setv self.population-histories
          (dfor species +species-list+ [(. species ["name"]) []]))
    (setv self.carrion-production carrion-production
          self.leaf-production leaf-production
          self.grass-production grass-production
          self.seed-production seed-production
          self.detritus-production detritus-production
          self.coconut-production coconut-production
          self.algae-production algae-production
          self.lichen-production lichen-production)
    (setv self.carrion 0
          self.leaves 0
          self.grass 0
          self.seeds 0
          self.detritus 0
          self.coconuts 0
          self.algae 0
          self.lichen 0))
  (defn --repr-- [self] self.identifier)
  (defn peak-population-single-species [self]
    (max (map max (.values self.population-histories))))
  (defn history-length [self]
    (max (map len (.values self.population-histories))))
  (defn grow [self]
    (+= self.carrion self.carrion-production)
    (+= self.leaves self.leaf-production)
    (+= self.grass self.grass-production)
    (+= self.seeds self.seed-production)
    (+= self.detritus self.detritus-production)
    (+= self.coconuts self.coconut-production)
    (+= self.algae self.algae-production)
    (+= self.lichen self.lichen-production))
  (defn roam [self]
    (setv roamers [])
    (for [i (range (dec (len self.population)) -1 -1)]
      (setv speed (. self.population [i] ["speed"]))
      (if speed
        (do
          (if (< (random.random)
                 (/ speed 10000))
            (.append roamers (.pop self.population i))))))
    roamers))

(setv biomes
      [(Biome "Grassland" :carrion-production 0 :leaf-production 100 :grass-production 1000 :seed-production 2000 :detritus-production 0 :coconut-production 0 :algae-production 0 :lichen-production 50)
       (Biome "Rainforest" :carrion-production 0 :leaf-production 1000 :grass-production 2000 :seed-production 100 :detritus-production 0 :coconut-production 0 :algae-production 0 :lichen-production 50)
       (Biome "Temperate Forest" :carrion-production 0 :leaf-production 2000 :grass-production 100 :seed-production 1000 :detritus-production 0 :coconut-production 0 :algae-production 0 :lichen-production 50)
       (Biome "Ocean" :carrion-production 10 :leaf-production 0 :grass-production 0 :seed-production 0 :detritus-production 10 :coconut-production 0 :algae-production 10000 :lichen-production 0)
       (Biome "Benthic" :carrion-production 10 :leaf-production 0 :grass-production 0 :seed-production 0 :detritus-production 1000 :coconut-production 0 :algae-production 0 :lichen-production 0)
       (Biome "Tundra" :carrion-production 1 :leaf-production 1 :grass-production 1 :seed-production 1 :detritus-production 1 :coconut-production 0 :algae-production 0 :lichen-production 300)
       (Biome "Desert" :carrion-production 100 :leaf-production 0 :grass-production 1 :seed-production 1 :detritus-production 1 :coconut-production 0 :algae-production 0 :lichen-production 0)
       (Biome "Shore" :carrion-production 0 :leaf-production 0 :grass-production 0 :seed-production 0 :detritus-production 20 :coconut-production 1000 :algae-production 1000 :lichen-production 10)
       (Biome "River" :carrion-production 500 :leaf-production 500 :grass-production 500 :seed-production 500 :detritus-production 500 :coconut-production 500 :algae-production 500 :lichen-production 500)
       (Biome "Human Garbage Dump" :carrion-production 100 :leaf-production 100 :grass-production 100 :seed-production 100 :detritus-production 100 :coconut-production 100 :algae-production 100 :lichen-production 100)])

(setv +num-generations+ 1500) ;; I will manually increase this for the real game.
(defn simulate []
  (setv generation 0)
  (while (< generation +num-generations+)
    (setv roamers [])
    (print)
    (print "Generation" generation)
    (for [biome biomes]
      (for [species +species-list+]
        (setv population-dict (population-to-dict biome.population))
        (.append (. biome.population-histories [(. species ["name"])])
                 (if (in (. species ["name"]) population-dict)
                   (. population-dict [(. species ["name"])]) 0)))
      (print " " biome)
      (for [[species quantity] (.items (population-to-dict biome.population))]
        (print "  " species quantity))

      (.grow biome)
      (.extend roamers (.roam biome))
     
      (setv biome.population (encounters biome)))
    (for [roamer roamers]
      (setv destination (random.choice biomes))
      (if (can-survive? roamer destination)
        (.append (. destination  population)
                 roamer)))
    (+= generation 1))

  ;; Display Populations Graph
  (import [matplotlib.pyplot :as plt])
  (setv [fig axs] (plt.subplots (len biomes)))
  (setv fig.suptitle "Multiple Biomes")
  (.plot (. axs [0]))
  (setv x-max (max (map (fn [biome] (.history-length biome))
                        biomes)))
  (setv y-max (max (map (fn [biome] (.peak-population-single-species biome))
                        biomes)))
  (for [[i biome] (enumerate biomes)]
    (.set-title (. axs [i]) biome.identifier)
    (for [[species history] (.items biome.population-histories)]
      (.set-xlim (. axs [i]) :left 0 :right x-max)
      ;(.set-ylim (. axs [i]) :bottom 1 :top y-max)
      (.plot (. axs [i])
             (range (len history)) history
             :label species)))
  (plt.legend)
  (plt.show))

(simulate)

